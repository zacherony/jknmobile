module.exports = class OperasiModel{
    
    constructor(req){
        this.request = req
    }

    generateListKodeBooking() {
        
        const resp = {
                        "list" : [{
                                "kodebooking": "123456ZXC",
                                "tanggaloperasi": "2019-12-11",
                                "jenistindakan": "operasi gigi",
                                "kodepoli": "001",
                                "namapoli": "Poli Bedah Mulut",
                                "terlaksana": 1,
                                "nopeserta": "0000000924782",
                                "lastupdate": 1577417743000 
                        },
                        {
                                "kodebooking": "67890QWE",
                                "tanggaloperasi": "2019-12-11",
                                "jenistindakan": "operasi mulut",
                                "kodepoli": "001",
                                "namapoli": "Poli Bedah Mulut",
                                "terlaksana": 0,
                                "nopeserta": "",
                                "lastupdate": 1577417743000
                        }]
                    }

        return resp
    }

    generateListJadwalOperasi() {
        const resp = {
            "list" : [{
                    "kodebooking": "123456ZXC",
                    "tanggaloperasi": "2019-12-11",
                    "jenistindakan": "operasi gigi",
                    "kodepoli": "001",
                    "namapoli": "Poli Bedah Mulut",
                    "terlaksana": 0 
            }]
        }

        return resp    
    }



}
