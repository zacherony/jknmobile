module.exports = class AntrianModel{
    constructor(req){
        this.request = req
    }

    generateAntrian() {
        
        const resp = {
                        "nomorantrean" : "A10",
                        "kodebooking" : "QWERTYUIO123",
                        "jenisantrean" : 2,
                        "estimasidilayani" : "1574985495848",
                        "namapoli" : "Poli Dalam",
                        "namadokter" : "",
                    }
        return resp
    }

    generateRekapan() {
        const resp = {
            "namapoli" : "Poli Jantung",
            "totalantrean" : 100,
            "jumlahterlayani" : 46,
            "lastupdate" : 1576040301000
        }        

        return resp    
    }



}
