const jwt = require('jsonwebtoken');
const response = require('../res')

module.exports = {
    /* isAuth hanya menge-cek apakah token expired atau tidak */
  isAuth: (req,res,next) => {
    try {
      const token = req.headers['x-token'];
      
      var decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, { expiresIn: `${process.env.ACCESS_TOKEN_EXPIRED}` } )
      req.user = decoded;
      
      next();
    } catch(err) {
        response.noAuth( "Token is Invalid or Expired" ,res)
      res.status(401).json({
        message: 'Token is Invalid'
      });
    }
  },
    /* isAuthorized menge-cek apakah token expired atau tidak dan juga mengecek apakah user pass valid */  
  isAuthorized: (req,res,next) => {
    const token = req.headers['x-token']
    try {
        var decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, { expiresIn: `${process.env.ACCESS_TOKEN_EXPIRED}` } )
        req.user = decoded      
        if(req.user.user == process.env.user && req.user.pass == process.env.pass) {
          next()
        }
    } catch (error) {
      if (error.name =="TokenExpiredError"){
        response.noAuth( "Token is Invalid or Expired" ,res)
      }
    }

  }

};

