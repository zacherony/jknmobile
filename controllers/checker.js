module.exports.cekParam = ( jenis, params ) => {

    let valid = true
    let message = ""

    if( jenis == "getantrian" ) {
            if( params.nomorkartu == "" || params.nomorkartu == undefined || params.nomorkartu.length !== 13  ){
                valid = false; message = "nomor kartu"

            } else if( params.nik == "" || params.nik == undefined || params.nik.length !== 16 ){
                valid = false; message = "nik"

            } else if( params.notelp == "" || params.notelp == undefined ){
                valid = false; message = "no telp"

            } else if( params.tanggalperiksa == "" || params.tanggalperiksa == undefined ){
                valid = false; message = "tanggal periksa"

            } else if( params.kodepoli == "" || params.kodepoli == undefined ){
                valid = false; message = "kode poli"

            } else if( params.nomorreferensi == "" || params.nomorreferensi == undefined ){
                valid = false; message = "nomor ref"

            } else if( isNaN(params.jenisreferensi) || params.jenisreferensi == undefined || params.jenisreferensi == "" ){
                valid = false; message = "jenis ref"

            } else if( isNaN(params.jenisrequest) || params.jenisrequest == undefined || params.jenisrequest == "" ){
                valid = false; message = "jenis request"

            } else if( isNaN(params.polieksekutif ) || params.polieksekutif == undefined  ){
                valid = false; message = "poli eksekutif"

            } else { valid = true; message = "" }
            
            return { "valid" : valid, "message" : message }


    } else if( jenis == "getrekap" ) {

            if( params.tanggalperiksa=="" || params.tanggalperiksa == undefined  ) {
                valid = false; message = "tanggal periksa"

            } else if( params.kodepoli=="" || params.kodepoli == undefined  ) {
                valid = false; message = "kode poli"

            } else if( isNaN( params.polieksekutif ) || params.polieksekutif == undefined  ) {
                valid = false; message = "poli eksekutif"                

            } else { valid = true; message = "" }

            return { "valid" : valid, "message" : message }

    } else if( jenis == "getlistbook" ) {

        if( params.nopeserta=="" || params.nopeserta == undefined ) {
            valid = false; message = "no peserta"

        } else { valid = true; message = "" }

        return { "valid" : valid, "message" : message }

    } else if( jenis == "getlistjadwal" ) {

        if( params.tanggalawal=="" || params.tanggalawal == undefined  ) {
            valid = false; message = "tanggal awal"

        } else if( params.tanggalakhir=="" || params.tanggalakhir == undefined  ) {
            valid = false; message = "tanggal akhir"

        } else { valid = true; message = "" }

        return { "valid" : valid, "message" : message }
    }    


}