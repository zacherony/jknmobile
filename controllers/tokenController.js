'use strict';

const jwt = require('jsonwebtoken')
const response = require('../res');
// var connection = require('./conn');

exports.generateAccessToken = function(req, res) {
    const username = req.body.username
    const password = req.body.password
    const user = { user: username, pass: password }

    // res.json(user)
    if( cekUserPass( username, password) ) {
        response.createTokenSukses( 
            jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: `${process.env.ACCESS_TOKEN_EXPIRED}` } ), res )
            // jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '30s' } ), res )
    } else {
        response.noAuth("anda tidak memiliki akses, silahkan hubungi admin", res)
    }
}

const cekUserPass = ( user, pass ) => {
    return ( user == process.env.user && pass == process.env.pass ) ? true : false
}