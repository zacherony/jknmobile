'use strict';

const { cekParam } = require('./checker')
const response = require('../res');
const model = require('../models/operasiModel')
const mod = new model()
// var connection = require('./conn');

exports.ambilKodeBooking = (req, res) => {
	let params = req.body

    // cek parameter apakah sesuai
    let isParam = cekParam('getlistbook', params )

    if( isParam.valid ) {
        response.ok( mod.generateListKodeBooking() ,res)

    } else {
        response.paramSalah( isParam.message + " tidak sesuai, silahkan cek parameter anda", res)

    }
    
}

exports.ambilJadwalOp = (req, res) => {
	let params = req.body

    // cek parameter apakah sesuai
    let isParam = cekParam('getlistjadwal', params )

    if( isParam.valid ) {
        response.ok( mod.generateListJadwalOperasi() ,res)

    } else {
        response.paramSalah( isParam.message + " tidak sesuai, silahkan cek parameter anda", res)
        
    }
    
}
