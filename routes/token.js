const token = require('../controllers/tokenController')
const response = require('../res')


module.exports = function(app) {

    app.route('/getToken')
        .post( token.generateAccessToken )

}