const opr = require('../controllers/operasiController')
const auth = require('../middlewares/auth')

module.exports = function(app) {

    app.post( '/getListBookOperasi', auth.isAuthorized, opr.ambilKodeBooking )

    app.post( '/getListJadwalOperasi', auth.isAuthorized, opr.ambilJadwalOp )    



}