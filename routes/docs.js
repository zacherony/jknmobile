const docs = require('../controllers/docs')
const response = require('../res')

module.exports = function(app) {

    app.route('/')
        .get(docs.index)

        














    // route ini harus berada paling akhir,
    // semua endpoint yang salah / tidak ketemu akan di handle oleh 
    // route ini.
    app.use((req, res, next)=> {
        response.notFound("Endpoint tidak ditemukan", res)
    })
        
}