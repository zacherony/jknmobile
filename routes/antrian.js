const antrian = require('../controllers/antrianController')
const auth = require('../middlewares/auth')

module.exports = function(app) {

    app.post( '/getNoAntrian', auth.isAuthorized, antrian.getNoAntrian )

    app.post( '/getRekapAntrian', auth.isAuthorized, antrian.getRekapAntrian )    

}