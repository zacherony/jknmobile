# jknMobile

Bridging JKNMobile adalah bridging antara SIMRS dengan JKNMobile, menjadi salah satu persyaratan bagi RS untuk bisa
kerjasama dengan BPJS. Bridging ini berada di sisi RS, pihak RS membuat Web Service menggunakan REST API. yang nantinya
akan menerima inputan data dari JKNMobile

Memakai Bahasa pemrogramman NODEJS, sebagai alternatif untuk tutorial bridging RS, yang kebanyakan memakai bahasa PHP dan
Visual Basic.

Dokumentasi :
[Katalog bridging jknMobile](https://docs.google.com/spreadsheets/d/1AL8bvvrPXRerKI8gT-o60UugcVwXCER-PjqKmQeLpKo/edit#gid=0)


### REST api
List of routes:

| Route           | HTTP    | Descriptions                    |
| :-------------  | :------ | :------------------------------ |
| `/`    | GET     | GET welcome response               |
| `/getToken`    | POST     | generate token               |
| `/getNoAntrian`    | POST     | GET nomor antrian               |
| `/getRekapAntrian`    | POST     | GET rekapan nomor antrian               |
| `/getListBookOperasi`    | POST     | GET kode booking operasi yang belum terlaksana               |
| `/getListJadwalOperasi`    | POST     | GET Mengambil jadwal operasi harian               |

---

### Config

open folder config, and edit .env file


| Items | Penjelasan |
| ------ | ------ |
| port | port number web service |
| user | username bpjs |
| pass | password bpjs |
| ACCESS_TOKEN_SECRET | secret key untuk generate token |
| ACCESS_TOKEN_EXPIRED | jumlah detik token akan expired cth: 900s |




### Usage
```
With only npm:

npm install
npm run devMain
```


### Example
```
sebagai client bisa memakai postman untuk mengakses API ( Lihat katalog di atas untuk melihat parameter )

atau 

buka project menggunakan vscode
install extension REST Client buatan Huachao Mao
di root folder project buka file `request.rest`

```