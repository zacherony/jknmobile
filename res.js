'use strict';

exports.createTokenSukses = (values, res) => {
  var data =    {
                    "response" : { "token" : values },
                    "metadata" : { "message" : "Ok", "code" : 200 }
                } 

  res.json(data);
  res.end();
};

exports.ok = function(values, res) {
    var data = {
        "response" : values,
        "metadata" : { "message" : "Ok", "code" : 200 }
    }

    res.status(200).json(data);    
    res.end();
  };

exports.noAuth = function(values, res) {
    var data = {
        "response" : { "data" : null },
        "metadata" : { "message" : values, "code" : 401 }
    }

    res.status(401).json(data);
    res.end();
}

exports.notFound = function(values, res) {
  var data = {
      "response" : { "data" : null },
      "metadata" : { "message" : values, "code" : 404 }
  }

  res.status(404).json(data);
  res.end();
}

exports.paramSalah = function(values, res) {
  var data = {
      "response" : { "data" : null },
      "metadata" : { "message" : values, "code" : 422 }
  }

  res.status(422).json(data);
  res.end();
}

  exports.antrianFormat = function(values, res) {
    var data = {
        "response" : { 
                      "nomorantrean" : values.noAntri, 
                      "kodebooking" : values.kdBooking, 
                      "jenisantrean" : values.jenisAntri, 
                      "estimasidilayani" : values.estimasi, 
                      "namapoli" : values.nmPoli, 
                      "namadokter" : values.nmDokter, 
        },
        "metadata" : { "message" : values, "code" : 401 }
    }
    res.json(data);
    res.end();
  };
