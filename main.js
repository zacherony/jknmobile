const {resolve} = require('path')

require('dotenv').config({path: resolve(__dirname,"./config/.env")})
// require('dotenv').config()

const express = require('express')
const app = express()
const cors = require('cors');
const port = process.env.port || 3000
const jwt = require('jsonwebtoken')

app.use(cors());
app.use( express.json() )

require('./routes/token')(app);
require('./routes/antrian')(app);
require('./routes/operasi')(app);
require('./routes/docs')(app);


// listen request at port
app.listen(port, () => console.log(`Server bridging, listening on port ${port}!`))
